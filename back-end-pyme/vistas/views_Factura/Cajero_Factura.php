<?php
include "../../Modelo/modelo_factura.php";
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Ventana Factura</title>
<!-- Bootstrap -->
<link href="../css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="../css/bootstrap.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- start plugins -->
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script src="js/jquery-1.11.3.min.js"></script>
	
<style type="text/css">
	.alert {
	
	display: none;
	 font-size: 12px;
	   box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
           
            padding: 10px;
	}
	.alert-danger, .alert-info{
	
	
	
	}
	        .errores{
            -webkit-boxshadow: 0 0 10px rgba(0, 0, 0, 0.3);
            -moz-box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
            -o-box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
            background: blue;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
            color: #fff;
            display: none;
            font-size: 12px;
            margin-top: -50px;
            margin-left: 200px;
            padding: 10px;
            position: absolute;
        }
	</style>
	
	<script src="js/jquery-1.11.3.min.js"></script>
	
	<script>
	var expr3 = /^[a-zA-Z]+$/;
	//Expresi�n para validar un correo electr�nico
    var expr = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    //Expresi�n para validar edad de 18 a 60 a�os
    var expr2 = /^1[8-9]|[2-5]\d|60$/;
    //validamos en telefono
    var expr4= /^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/;

    var expr6=/^(\d){8}$/;
	$(document).ready(function() {
		
		$('#comprobar').click(function(event) {
			$("#msgNom").fadeOut();
			$("#msgApe").fadeOut();
			$("#msgEdad").fadeOut();
			$("#msgEma").fadeOut();
			$("#msgdirec").fadeOut();
			$("#msgtele").fadeOut();
			$("#msgdni").fadeOut();
			
			
			var nickVar = $('#nick').val();
			 if(nickVar.trim() == "" ){
                 $("#msgNick").fadeIn("slow");
                 return false;
             }
             else{
            	 $("#msgNick2").fadeOut();
            	 $.post('comprobarUser', {
						nick: nickVar
					}, function(responseText) {
						$("#msgNick2").fadeIn("slow");
						$('#msgNick2').html(responseText);
					});
            	 
                 }
			
		});//click
		 
		$('#registrar').click(function(event) {
			$("#msgNom").fadeOut();
			$("#msgApe").fadeOut();
			$("#msgEdad").fadeOut();
			$("#msgEma").fadeOut();
			$("#msgdirec").fadeOut();
			$("#msgtele").fadeOut();
			$("#msgdni").fadeOut();
			
			
			
			
			var nombreVar = $('#nomempleado').val();
			var apellidoVar = $('#passwordempleado').val();
			var edadVar = $('#edadempleado').val();
			var emailVar = $('#corempleado').val();
			var nickVar = $('#dirempleado').val();
			var passVar = $('#telempleado').val();
			var rpassVar = $('#dniempleado').val();
			
			 // --- Condicionales anidados ----
            //Si nombre est� vac�o
            //Muestra el mensaje
            //Con false sale de los if's y espera a que sea pulsado de nuevo el bot�n de enviar
            if(nombreVar.trim() == "" || !expr3.test(nombreVar)){
                $("#msgNom").fadeIn("slow");
                return false;
            }
            //en otro caso, el mensaje no se muestra
            else{
                $("#msgNom").fadeOut();

                //Si correo est� vac�o y la expresi�n NO corresponde -test es funci�n de JQuery
                //Muestra el mensaje
                //Con false sale de los if's y espera a que sea pulsado de nuevo el bot�n de enviar
                if(apellidoVar.trim() == ""){
                    $("#msgApe").fadeIn("slow");
                    return false;
                }
                else{
                    $("#msgApe").fadeOut();

                    if(edadVar.trim() == "" || !expr2.test(edadVar)){
                        $("#msgEdad").fadeIn("slow");
                        return false;
                    }
                    else{
                        $("#msgEdad").fadeOut();

                        if(emailVar.trim() == "" || !expr.test(emailVar)){
                            $("#msgEma").fadeIn("slow");
                            return false;
                        }else{
                        	$("#msgEma").fadeOut();
                        	
                        	if(nickVar.trim() == ""){
                        		$("#msgdirec").fadeIn("slow");
                        		return false;
                        	}else{
                        		$("#msgdirec").fadeOut();
                        		if(passVar.trim() == "" || !expr4.test(passVar)){
                        			$("#msgtele").fadeIn("slow");
                        			return false;
                        		}else{
                        			$("#msgtele").fadeOut();
                        			if(rpassVar.trim() == "" || !expr6.test(rpassVar)){
                            			$("#msgdni").fadeIn("slow");
                            			return false;
                            		}else{
                            			$("#msgdni").fadeOut();

                            			
                        			// Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get
                    					$.post('crearUsuario', {
                    						nombre : nombreVar,
                    						apellido: apellidoVar,
                    						edad: edadVar,
                    						email: emailVar,
                    						nick: nickVar,
                    						password: passVar
                    					}, function(responseText) {
                    						$("#respuesta").fadeIn("slow");
                    						$('#respuesta').html(responseText);
                    						$(":text").each(function(){	
                    							$($(this)).val('');
                    					});
                    						$(":password").each(function(){	
                    							$($(this)).val('');
                    					});
                    					});
                            		}
                        			
                        		}
                        	}
                        }
                    }
                }
            }
	

        });//click
    });//ready
			
</script>
		 <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 100%;
      margin: auto;
  }

  /*  bhoechie tab */

div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
  </style>
<!----font-Awesome----->
   	<link rel="stylesheet" href="../fonts/css/font-awesome.min.css">
<!----font-Awesome----->
</head>
<body>
<div class="header_bg1">
<div class="container">
	<div class="row header">
		<div class="logo navbar-left">
			<h1><a href="inicio.html">EL DRAGONCITO </a></h1>
		</div>
		
		<div class="clearfix"></div>
	</div>
	<div class="row h_menu">
		<nav class="navbar navbar-default navbar-left" role="navigation">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li ><a href="../inicio.html">INICIO</a></li>
		        <li><a href="menu.html">MENUS</a></li>
		        <li><a href="conocenos.html">CONOCENOS</a></li>
		        <li   ><a href="calificanos.html">CALIFICANOS</a></li>
		        <li><a href="contactos.html">CONTACTANOS</a></li>
		        <li class="active" ><a href="Cajero_Factura.php">CAJERO</a></li>

		      </ul>
		    </div><!-- /.navbar-collapse -->
		    <!-- start soc_icons -->
		</nav>
		
	</div>
	<div class="clearfix"></div>
</div>
</div>
<!--Perfil-->
	<section class="main-ini container">
		<div class="row">
			<br>
			<section class="posts col-md-4">
				
				 <div class="profile-header-container">   
    				<div class="profile-header-img">
                		<img class="img-circle" src="../images/c1.jpg" />
                	<!-- badge -->
                	<div class="rank-label-container">
                    	<span class="label label-default rank-label">Cajero....</span>
                	</div>
            		</div>
        		</div>
			</section>
			<section class="posts col-md-8">
				<h2>FACTURA</h2>
			</section>
		</div>
	</section>
	<!--Perfil-->

	<br>

	<br>

	<section class="main-ini container">
	
		<div class="row">
			<div class="posts col-md-12 bhoechie-tab-container">
	            <div class="posts col-md-2 bhoechie-tab-menu">
	              <div class="list-group">
	                <a href="#" class="list-group-item  text-center active">
	                  <br/>Factura
	                </a>
	              </div>
	        	</div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 bhoechie-tab">
                <!-- flight section --> 
                <!-- train section -->
                <!-- hotel search -->
                
                <!-- Contenido aqui va todoo -->                    
                	 <div class="content-wrapper">
						<!-- Main content -->
						<section class="content">
							<div class="row">
								<div class="col-md-12">
									<div class="box">
										<div class="box-header with-border">
											<div class="box-tools pull-right">
												
											</div>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<div class="row">
												<div class="col-md-12">
			
													<section class="content">
														<div class="row">
															<!-- PRIMERA COLUMNA  ROBERT ...............................-->
															<!-- left column -->
															<div class="col-md-4">
																<!--Contenido-->
																<div class="box box-primary">
																	<div class="box-header with-border">
																		
																	</div>
																	<!-- /.box-header -->
																	<!-- form start -->
																	<form accept-charset="utf-8" action="../../controlador/controlador_factura.php" method= "Post">
																		<div class="box-body">
																		
																			<div class="form-group">
																				<input type="hidden" name="crud" value="1"> <label
																					for=" ">Nombre del Cliente</label> <input
																					type="text" name="nomCliente" class="form-control"
																					id="nomCliente" placeholder="Nombre del Cliente">
																			</div>
																			
																			<div id="msgNom" class="alert alert-danger alert-dismissable">
  																				<strong>Error!</strong> Ingrese un nombre.
																				</div>	
																		
																			<div class="form-group">
																				<label
																				 for=" ">D�a:</label> <input
																					type="text" name="dia" class="form-control"
																					id="dia" placeholder="Ingrese d�a">
																			</div>
																			<div id="msgEdad" class="alert alert-danger alert-dismissable"><strong>Error!</strong>Ingrese edad entre 18 y 60 (Solo numeros).
																				</div>	
																			
																			<div class="form-group">
																				<label
																				 for=" ">Mes:</label> <input
																					type="text" name="mes" class="form-control"
																					id="mes" placeholder="Ingrese mes">
																			</div>
																			<div id="msgEdad" class="alert alert-danger alert-dismissable"><strong>Error!</strong>Ingrese edad entre 18 y 60 (Solo numeros).
																				</div>
																				
																			<div class="form-group">
																				<label
																				 for=" ">A�o:</label> <input
																					type="text" name="anio" class="form-control"
																					id="anio" placeholder="Ingrese anio">
																			</div>
																			<div id="msgEdad" class="alert alert-danger alert-dismissable"><strong>Error!</strong>Ingrese edad entre 18 y 60 (Solo numeros).
																				</div>
																			
																			<label
																				 for=" ">Empleado</label>
																			<select class="form-control" name="tipoempleado" id="tipoempleado">
																			    <?php
																			    $con1 = conectar();
																			    $query1 = "SELECT * FROM empleado where tipo_cargo_TipCarCod = 2";
																			    $resultado1 = mysqli_query ($con1, $query1);
																			    ?>
																			    <?php
																			    while ( $fila = mysqli_fetch_array ( $resultado1 ) ) {?>
																			    <option value=<?php echo $fila['EmpCod'];?>><?php echo $fila['EmpNom'];?> </option>
																			   <?php
																				}
																				?>
																			 </select>
																		</div>
																		<!-- /.box-body -->
			
																		<div class="box-footer">
																			<button type="submit" class="btn btn-primary">AGREGAR</button>
																		</div>
																	</form>
																	
																</div>
															</div>
			
			
															<!-- SEGUNDA COLUMNA  ROBERT .............................................-->
															<div class="col-md-8">
			
																<div class="box-body table-responsive no-padding">
			
																	<table class="table table-bordered text-center">
																		<tr>
												
																
																			<th>N�mero de Factura</th>
																			<th>Nombre Cliente</th>
																			<th>Igv</th>
																			<th>Dia</th>
																			<th>Mes</th>
																			<th>A�o</th>
																			<th>Codigo Empleado</th>
																			<th>Estado</th>
																		</tr>
			                <?php										
														             
																	    $resultado=MostrarFactura();
 
																		while ( $fila = mysqli_fetch_array ( $resultado ) ) {
																				?>		
																		<tr>
																			<td><?php echo $fila['FacCod'];?></td>
																			<td><?php echo $fila['FacNomCli'];?></td>
																			<td><?php echo $fila['FacIgv'];?></td>
																			<td><?php echo $fila['FacFecDia'];?></td>
																			<td><?php echo $fila['FacFecMes'];?></td>
																			<td><?php echo $fila['FacFecAnio'];?></td>
																			<td><?php echo $fila['empleado_EmpCod'];?></td>
																			<td><?php echo $fila['Estado_Registro_EstRegCod'];?></td>
																			<td>
																				<button type="button"
																					onClick="eliminarEmpleado(<?php echo $fila['FacCod'];?>)"
																					class='btn btn-block btn-danger'>Eliminar</button>
																			</td>
																			<td><a href="#" class="btn btn-success"
																				data-toggle="modal" data-target="#modal-success"
																				onClick="modificarFactura('<?php echo $fila['FacCod']?>','<?php echo $fila['FacNomCli'];?>','<?php echo $fila['FacIgv']?>',
																				'<?php echo $fila['FacFecDia']?>','<?php echo $fila['FacFecMes']?>','<?php echo $fila['FacFecAnio']?>',
																				'<?php echo $fila['empleado_EmpCod']?>','<?php echo $fila['Estado_Registro_EstRegCod']?>');">Modificar
																			</a></td>
																		</tr>
																							
																 <?php			
																 
																		}
																			
																			?>
																			
							<script>
							function modificarFactura(FacCod,FacNomCli,FacIgv,FacFecDia,FacFecMes,FacFecAnio,empleadoEmpCod,estado){
								$('#mFacCod').val(FacCod);
								$('#mEmpCli').val(FacNomCli);
								$('#mFacIGV').val(FacIgv);
								$('#mFacDia').val(FacFecDia);
								$('#mEmpCor').val(FacFecMes);
								$('#mFacAnio').val(FacFecAnio);
								$('#mEmpCod').val(empleadoEmpCod);
							}
						
							</script>
																		<script>
							$('#mbtnUpdateEmpleado').click(function(){
							var respuesta=	confirm("Estas seguro de eliminar este empleado");
										$('#mbtnCerradoModal').click();
											
							});		
							</script>
																		<script>
							 function eliminarEmpleado(id_Factura){
								var respuesta = confirm("Estas seguro de eliminar este empleado");
								if(respuesta){
									window.location.href="../../controlador/controlador_Factura.php?idFactura="+idFactura+"&crud=2";
								}
							 } 				 
							 function modificarTipoPlato(){
								 function myFunction() {
									    document.getElementById("data-target='#modal-success'").MostrarFactura();
									}					 			
										//window.location.href="?id_tipoplato="+id_tipoplato;
							} 
			
							 $(document).ready(function(){
								    $("#myBtn").click(function(){
								        $("#modal-success").modal();
								    });
								});
							</script>
																	</table>
																</div>
															</div>
													</div>
													</section>
													<!-- ESTA PARTE ES PARA MODIFICAR  INCIAA  -->
			
													<div class="modal modal-success fade" id="modal-success">
														<div class="modal-dialog">
															<form role="form" action="../../controlador/controlador_Factura.php"
																method="POST">
																<div class="modal-content">
																	<div class="modal-header">
																		<button type="button" class="close" data-dismiss="modal"
																			aria-label="Close">
																			<span aria-hidden="true">&times;</span>
																		</button>
																		<h4 class="modal-title">Success Modal</h4>
																	</div>
																	<div class="modal-body">
																		<div class="box-body">
																			<div class="form-group">
																				<input type="hidden" name="mFacCod" id="mFacCod"> <input
																					type="hidden" name="crud" value="3"> <label
																					for=" "> Nombre del Cliente</label> <input
																					type="text" name="mEmpCli" class="form-control"
																					id="mEmpCli">
																			</div>

																				<div class="form-group">
																				<label for=" ">Igv</label> <input
																					type="text" class="form-control" name="mFacIGV"
																					id="mFacIGV" placeholder="">
																			</div>

																			<div class="form-group">
																				<label for=" ">Dia</label> <input
																					type="text" class="form-control" name="mFacDia"
																					id="mFacDia" placeholder="">
																			</div>

																			<div class="form-group">
																				<label for=" ">Mes</label> <input
																					type="text" class="form-control" name="mEmpCor"
																					id="mEmpCor" placeholder="">
																			</div>

																			<div class="form-group">
																				<label for=" ">Anio</label> <input
																					type="text" class="form-control" name="mFacAnio"
																					id="mFacAnio" placeholder="">
																			</div>

																			<label
																				 for=" ">Tipo de empleado</label>
																			<select class="form-control" name="mEmpCod" id="mEmpCod">
																			    <?php
																			    $resultado1 =mysqli_query ($con1, $query1);
																			    while ( $fila = mysqli_fetch_array ( $resultado1 ) ) {?>
																			    <option value=<?php echo $fila['EmpCod'];?>><?php echo $fila['EmpNom'];?> </option>
																			   <?php
																							}
																							?>
																			 </select>
																		</div>
																		<!-- /.box-body -->
			
																	</div>
																	<div class="modal-footer">
																		<button type="button" id="mbtnCerradoModal"
																			class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
																		<button type="submit" id="mbtnUpdateEmpleado"
																			class="btn btn-outline">Guardar Cambios</button>
																	</div>
			
																</div>
															</form>
															<!-- /.modal-content -->
														</div>
														<!-- /.modal-dialog -->
													</div>
			
													<!-- fin de  la modificacionnnnn  -->
			
												</div>
											</div>
			
										</div>
									</div>
									<!-- /.row -->
								</div>
								<!-- /.box-body -->
							</div>
							<!-- /.box -->
					</section>
					</div>    
				<!--  -->

            </div>
        </div>		
		</div>
</section>			
			
<div class="footer_bg"><!-- start footer -->
	<div class="container">
		<div class="row  footer">
			<div class="copy text-center">
				<p class="link"><span>&#169; reserva | ssss&nbsp;<a href="#"> el dragoncito</a></span></p>
			</div>
		</div>
	</div>
</div>
</body>
</html>